from django.shortcuts import render, redirect

# Create your views here.
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm
from receipts.forms import AccountForm


@login_required
def receipts_home(request):
    irrelevant = Receipt.objects.filter(purchaser=request.user)
    context = {
        "elmo": irrelevant,
    }
    return render(request, "receipts/list.html", context)


def redirect_to_receipt_list(request):
    return redirect("home")


@login_required(login_url="/accounts/login/")
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def create_expenseCategory(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)


@login_required
def accounts_home(request):
    irrelevant = Account.objects.filter(owner=request.user)
    context = {
        "big_bird": irrelevant,
    }
    return render(request, "accounts/list.html", context)


@login_required
def categories_home(request):
    irrelevant = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "ernie": irrelevant,
    }
    return render(request, "categories/list.html", context)

