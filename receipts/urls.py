from django.urls import path
from receipts.views import receipts_home, create_receipt, categories_home
from receipts.views import accounts_home, create_expenseCategory
from receipts.views import create_account

urlpatterns = [
    path("", receipts_home, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", categories_home, name="category_list"),
    path("accounts/", accounts_home, name="account_list"),
    path("categories/create/", create_expenseCategory, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]